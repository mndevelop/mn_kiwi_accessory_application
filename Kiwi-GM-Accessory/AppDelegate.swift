//
//  AppDelegate.swift
//  Kiwi-GM-Accessory
//
//  Created by Amir Nazari on 6/22/18.
//  Copyright © 2018 MN Develop LLC. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseAuth
import FirebaseDatabase

var skippedLogin = false

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        
        if Auth.auth().currentUser != nil {
            // Go straight to game selection
            skippedLogin = true
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            if let mainVC = storyboard.instantiateInitialViewController() {
                self.window?.rootViewController = mainVC
                self.window?.makeKeyAndVisible()
            }
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        if let roomVC = UIApplication.topViewController() as? AccessoryViewController {
            roomVC.timerRef.child("connectedAccessoryCount").observeSingleEvent(of: .value) { (snapshot) in
                
                var count = 0
                
                if let roomCount = snapshot.value as? NSNumber {
                    count = roomCount.intValue - 1
                }
                
                roomVC.timerRef.child("connectedAccessoryCount").setValue(max(0, count))
                
                if count > 0 {
                    roomVC.timerRef.child("AccessoryConnected").setValue("true")
                } else {
                    roomVC.timerRef.child("AccessoryConnected").setValue("false")
                }
            }
        } else if let lockVC = UIApplication.topViewController() as? LockViewController {
            
            if let roomVC = lockVC.senderVC as? AccessoryViewController {
                roomVC.timerRef.child("connectedAccessoryCount").observeSingleEvent(of: .value) { (snapshot) in
                    
                    var count = 0
                    
                    if let roomCount = snapshot.value as? NSNumber {
                        count = roomCount.intValue - 1
                    }
                    
                    roomVC.timerRef.child("connectedAccessoryCount").setValue(max(0, count))
                    
                    if count > 0 {
                        roomVC.timerRef.child("AccessoryConnected").setValue("true")
                    } else {
                        roomVC.timerRef.child("AccessoryConnected").setValue("false")
                    }
                }
            }
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if let roomVC = UIApplication.topViewController() as? AccessoryViewController {
            roomVC.timerRef.child("connectedAccessoryCount").observeSingleEvent(of: .value) { (snapshot) in
                
                var count = 1
                
                if let roomCount = snapshot.value as? NSNumber {
                    count = roomCount.intValue + count
                }
                
                roomVC.timerRef.child("connectedAccessoryCount").setValue(count)
                roomVC.timerRef.child("AccessoryConnected").setValue("true")
            }
        } else if let lockVC = UIApplication.topViewController() as? LockViewController {
            
            if let roomVC = lockVC.senderVC as? AccessoryViewController {
                roomVC.timerRef.child("connectedAccessoryCount").observeSingleEvent(of: .value) { (snapshot) in
                    
                    var count = 1
                    
                    if let roomCount = snapshot.value as? NSNumber {
                        count = roomCount.intValue + count
                    }
                    
                    roomVC.timerRef.child("connectedAccessoryCount").setValue(count)
                    roomVC.timerRef.child("AccessoryConnected").setValue("true")
                }
            }
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
    
}

