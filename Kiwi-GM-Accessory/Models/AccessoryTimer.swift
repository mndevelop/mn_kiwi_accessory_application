//
//  AccessoryTimer.swift
//  Kiwi-GM-Accessory
//
//  Created by Amir Nazari on 6/28/18.
//  Copyright © 2018 MN Develop LLC. All rights reserved.
//

import Foundation
import UIKit

class AccessoryTimer {
    
    var key: String = ""
    var parentGame: String = ""
    var parentGameTimeLimit: TimeInterval = 0
    var title: String = ""
    var timerMessage: String = ""
    var defuseCode: String = ""
    var completionMessage: String = ""
    var isGameWinningCondition: String = ""
    var type: String = ""
    var themeImage: String = ""
    
    init() {
        
    }
    
    init(key: String, parentGame: String, title: String) {
        self.key = key
        self.parentGame = parentGame
        self.title = title
    }
}
