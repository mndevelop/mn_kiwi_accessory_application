//
//  UIViewExtensions.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 3/4/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func applyDropShadow(shadowColor: UIColor = .black,
                         masksToBounds: Bool = false,
                         opacity: Float = 0.5,
                         shadowOffsetHeight: Double = 2.0,
                         shadowOffsetWidth: Double = 0.0,
                         radius: CGFloat = 6.0) {
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = masksToBounds
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight)
        self.layer.shadowOpacity = opacity
        self.layer.cornerRadius = radius
        self.layer.shadowPath = shadowPath.cgPath
    }
}

extension UIColor {
    
    static func kiwiGrayTransparent() -> UIColor {
        return UIColor(displayP3Red: 170/255, green: 170/255, blue: 170/255, alpha: 0.7)
    }
    
    static func kiwiWhiteTransparent() -> UIColor {
        return UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 0.7)
    }
    
    static func kiwiGreen() -> UIColor {
        return UIColor(displayP3Red: 193/255, green: 220/255, blue: 74/255, alpha: 1)
    }
    static func kiwiRed() -> UIColor {
        return UIColor(displayP3Red: 255/255, green: 86/255, blue: 83/255, alpha: 1.0)
    }
    
    static func kiwiBlue() -> UIColor {
        return UIColor(displayP3Red: 33/255, green: 216/255, blue: 236/255, alpha: 1.0)
    }
    
    static func kiwiGold() -> UIColor {
        return UIColor(displayP3Red: 255/255, green: 181/255, blue: 0/255, alpha: 1.0)
    }
}
