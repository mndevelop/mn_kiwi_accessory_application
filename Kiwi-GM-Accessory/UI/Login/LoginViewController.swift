//
//  LoginViewController.swift
//  Kiwi-GM-Accessory
//
//  Created by Amir Nazari on 6/22/18.
//  Copyright © 2018 MN Develop LLC. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import Lottie

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    var ref: DatabaseReference!
    
    var userEmail = ""
    
    @IBOutlet weak var emailLoginField: UITextField!
    @IBOutlet weak var passwordLoginField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var kiwiLogoButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    var forgotPasswordMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        hideKeyboardWhenTappedAround()
        skippedLogin = false
        
        // Do any additional setup after loading the view.
//        signUpButton.applyDropShadow()
//        loginButton.applyDropShadow()
//
//        emailLoginField.applyDropShadow()
//        passwordLoginField.applyDropShadow()
//
        emailLoginField.text = userEmail
                
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= (keyboardSize.height - 125)
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailLoginField {
            passwordLoginField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            loginAction()
        }
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func forgotPasswordButtonPressed(_ sender: UIButton) {
        showForgotPassword()
    }
    
    func forgotPasswordConfirmAction() {
        // Attempt to send password reset email
        if areFieldsFilled(textfields: [emailLoginField]) {
            
            let email = emailLoginField.text!
            
            Auth.auth().sendPasswordReset(withEmail: email, completion: { (error) in
                if error != nil {
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    return
                } else {
                    let alertController = UIAlertController(title: "Email Request Sent", message: "Please check your email for further instructions.", preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: {
                        self.hideForgotPassword()
                    })
                    return
                }
            })
        } else {
            let alertController = UIAlertController(title: "Missing Information", message: "You must enter an email to reset password.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func forgotPasswordCancelAction() {
        hideForgotPassword()
    }
    
    func showForgotPassword() {
        
        emailLoginField.becomeFirstResponder()
        
        forgotPasswordMode = true
        forgotPasswordButton.isHidden = true
        passwordLoginField.isHidden = true
        loginButton.setTitle("Send Reset Password Email", for: .normal)
        signUpButton.setTitle("Cancel", for: .normal)
    }
    
    func hideForgotPassword() {
        forgotPasswordMode = false
        forgotPasswordButton.isHidden = false
        passwordLoginField.isHidden = false
        loginButton.setTitle("Login", for: .normal)
        signUpButton.setTitle("Sign Up", for: .normal)
    }
    
    
    @IBAction func selectTestAccountButtonPressed(_ sender: UIButton) {
        
        if _isDebugAssertConfiguration() {
            let alert = UIAlertController(title: "Select Account", message: "", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "kiwidevshop", style: .default, handler: { (action) in
                self.emailLoginField.text = "kiwidevshop@yahoo.com"
                self.passwordLoginField.text = "Coolio123"
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.sourceView = sender
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        
        if forgotPasswordMode {
            forgotPasswordCancelAction()
            return
        }
        
        if !areFieldsFilled(textfields: [emailLoginField, passwordLoginField]) {
            //Tells the user that there is an error
            let alertController = UIAlertController(title: "Missing Information", message: "One or more of the login fields is empty. Please make sure to enter your email and password to sign up or login.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        guard let email = emailLoginField.text else { return }
        guard let password = passwordLoginField.text else { return }
        
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    //Tells the user that there is an error and then gets firebase to tell them the error
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
            }
            
            if let userID = user?.uid {
                self.ref.child(userID).child("Game Limit").setValue(1)
            }
            
            self.login(email: email, password: password)
            
        })
    }
    
    func areFieldsFilled(textfields: [UITextField]) -> Bool {
        
        for field in textfields {
            guard let textInput = field.text else { return false }
            if textInput.isEmpty {
                return false
            }
        }
        
        return true
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        if forgotPasswordMode {
            forgotPasswordConfirmAction()
            return
        }
        
        loginAction()
    }
    
    func loginAction() {
        if !areFieldsFilled(textfields: [emailLoginField, passwordLoginField]) {
            //Tells the user that there is an error
            let alertController = UIAlertController(title: "Missing Information", message: "One or more of the login fields is empty. Please make sure to enter your email and password to sign up or login.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        guard let email = emailLoginField.text else { return }
        guard let password = passwordLoginField.text else { return }
        
        login(email: email, password: password)
    }
    
    func login(email: String, password: String) {
        // Do some login stuff here
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            if error != nil {
                
                DispatchQueue.main.async {
                    //Tells the user that there is an error and then gets firebase to tell them the error
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
            }
            
            if let _ = user {
                DispatchQueue.main.async {
                    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                    if let mainVC = storyboard.instantiateInitialViewController() {
                        
                        // At this point I would set up any necessary user information for load of the users games.
                        // Maybe use prepare for segue as well
                        self.passwordLoginField.text = ""
                        self.present(mainVC, animated: true, completion: nil)
                    }
                }
            }
        })
    }
    
}
