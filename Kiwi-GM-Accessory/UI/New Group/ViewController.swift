//
//  ViewController.swift
//  Kiwi-GM-Accessory
//
//  Created by Amir Nazari on 6/22/18.
//  Copyright © 2018 MN Develop LLC. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var accessoryTableView: UITableView!
    @IBOutlet weak var addAccessoryButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var editGameButton: UIButton!
    
    var editingGames = false
    
    var selectedAccessory: AccessoryTimer!
    var accessoryList: [AccessoryTimer] = []
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleUI()
        
        if let currentUserID = FirebaseUserManager().getCurrentUser()?.uid {
            
            ref = Database.database().reference().child("\(currentUserID)")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupGameObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeFirebaseObservers()
    }
    
    func removeFirebaseObservers() {
        ref.child("AccessoryTimers").removeAllObservers()
    }
    
    func styleUI() {
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }

    @IBAction func addAccessoryButtonPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toAddFormVC", sender: nil)
    }
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        
        if accessoryList.count == 0 { return }
        
        // Editing games
        editingGames = !editingGames
        if editingGames {
            editGameButton.setTitle("Done", for: .normal)
        } else {
            editGameButton.setTitle("Edit", for: .normal)
        }
        
        accessoryTableView.reloadData()
    }

    @IBAction func settingsButtonPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func logoutButtonPressed(_ sender: UIButton) {
        //  Show notification asking if user wants to logout
        if Auth.auth().currentUser != nil {
            do {
                
                if skippedLogin {
                    // instantiate login vc and add on navigation controller completion?
                    let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
                    guard let loginVC = storyboard.instantiateInitialViewController() else {
                        // error logging out
                        return
                    }
                    
                    if let email = Auth.auth().currentUser?.email {
                        (loginVC as? LoginViewController)?.userEmail = email
                    }
                    
                    try Auth.auth().signOut()
                    
                    UIApplication.shared.keyWindow?.rootViewController = loginVC
                } else {
                    try Auth.auth().signOut()
                    
                    self.navigationController?.dismiss(animated: true, completion: nil)
                }
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accessoryList.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: GameSelectionTableViewCell.reuseID) as? GameSelectionTableViewCell else {
            return UITableViewCell()
        }
        
        if self.editingGames {
            cell.deleteButton.isHidden = false
            let yellowColor = UIColor(red: 236/255, green: 250/255, blue: 58/255, alpha: 1.0)
            cell.roomButton.layer.backgroundColor = yellowColor.cgColor
        } else {
            cell.deleteButton.isHidden = true
            let greenColor = UIColor(red: 193/255, green: 220/255, blue: 74/255, alpha: 1.0)
            cell.roomButton.layer.backgroundColor = greenColor.cgColor
        }
        
        cell.roomButton.tag = indexPath.row
        cell.deleteButton.tag = indexPath.row
        cell.roomButtonLabel.text = accessoryList[indexPath.row].title
        cell.gameNameLabel.text = "Parent Game: \(accessoryList[indexPath.row].parentGame)"
        
        cell.roomButton.addTarget(self, action: #selector(roomButtonTapped(sender:)), for: .touchUpInside)
        cell.deleteButton.addTarget(self, action: #selector(deleteButtonTapped(sender:)), for: .touchUpInside)
        
        return cell
     }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    @objc func deleteButtonTapped(sender: UIButton) {
        selectedAccessory = accessoryList[sender.tag]

        let alert = UIAlertController(title: "Delete \(selectedAccessory.title)?", message: "This will PERMANENTLY DELETE \(selectedAccessory.title) and all of it's data. \n\nAre you sure you want to do this?", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes, Delete this game", style: .destructive, handler: { (alert) in
            self.ref.child("AccessoryTimers").child(self.selectedAccessory.key).removeValue()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func roomButtonTapped(sender: UIButton) {
        selectedAccessory = accessoryList[sender.tag]

        if editingGames {
            self.performSegue(withIdentifier: "toAddFormVC", sender: nil)
        } else {
            self.performSegue(withIdentifier: "toRoomVC", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        editGameButton.setTitle("Edit", for: .normal)
        accessoryTableView.reloadData()

        if let roomVC = segue.destination as? AccessoryViewController {
            roomVC.thisRoom = selectedAccessory
            roomVC.room = selectedAccessory.parentGame
        }

        if let addForm = segue.destination as? AddAccessoryFormViewController {
            if editingGames {
                addForm.accessoryToEdit = self.selectedAccessory
            }

            addForm.accessoryList = self.accessoryList
        }

//        if let settingsVC = segue.destination as? AppSettingsViewController {
//            settingsVC.accessoryList = accessoryList
//        }
        
        editingGames = false
    }
    
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) {
        print("Made it back successfully")
    }
    
    func setupGameObservers() {
        ref.child("AccessoryTimers").observe(.value, with: { (snapshot) in
            //if the reference have some values
            
            if snapshot.childrenCount > 0 {
                
                //clearing the list
                self.accessoryList.removeAll()
                
                //iterating through all the values
                for child in snapshot.children {
                    if let value = (child as? DataSnapshot)?.value as? [String : Any] {
                        
                        let accessory = AccessoryTimer()
                        
                        if let snap = child as? DataSnapshot {
                            accessory.key = snap.key
                        }
                        
                        guard let parent = value["parentGame"] as? String else {
                            return
                        }
                        
                        guard let title = value["title"] as? String else {
                            return
                        }
                        
                        accessory.parentGame = parent
                        accessory.title = title
                        
                        if let defuseCode = value["defuseCode"] as? String {
                            accessory.defuseCode = defuseCode
                        }
                        
                        if let timerMessage = value["timerMessage"] as? String {
                            accessory.timerMessage = timerMessage
                        }
                        
                        if let completionMessage = value["completionMessage"] as? String {
                            accessory.completionMessage = completionMessage
                        }
                        
                        if let type = value["type"] as? String {
                            accessory.type = type
                        }
                        
                        if let themeImage = value["themeImage"] as? String {
                            accessory.themeImage = themeImage
                        }
                        
                        if let gameWinningCondition = value["gameWinningCondition"] as? String {
                            accessory.isGameWinningCondition = gameWinningCondition
                        }
                        
                        self.accessoryList.append(accessory)
                    }
                }
                
                DispatchQueue.main.async {
//                    self.animationStackViewContainer.isHidden = true
                    self.accessoryTableView.isHidden = false
                }
            } else {
                self.accessoryList = []
                
                DispatchQueue.main.async {
//                    if let animatedView = self.animationView {
//                        animatedView.play()
//                    } else {
//                        self.playAnimation(view: self.animationView)
//                    }
//                    self.animationStackViewContainer.isHidden = false
                    self.accessoryTableView.isHidden = true
                    if self.editingGames {
                        self.editingGames = false
                    }
                    
                    self.editGameButton.setTitle("Edit", for: .normal)
                }
            }
            
            DispatchQueue.main.async {
                self.accessoryTableView.reloadData()
            }
        })
    }
}

