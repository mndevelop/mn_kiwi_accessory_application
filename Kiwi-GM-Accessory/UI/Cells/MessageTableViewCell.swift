//
//  MessageTableViewCell.swift
//  MN_Kiwi_GM
//
//  Created by Amir Nazari on 3/11/18.
//  Copyright © 2018 Mirabutaleb Nazari. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var messageLabel: UILabel!
    
    static let reuseID = "messageCellReuse"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
