//
//  AccessoryViewController.swift
//  Kiwi-GM-Accessory
//
//  Created by Amir Nazari on 6/22/18.
//  Copyright © 2018 MN Develop LLC. All rights reserved.
//

import UIKit
import FirebaseDatabase
import MZTimerLabel

class AccessoryViewController: UIViewController, UITextFieldDelegate {

    var ref: DatabaseReference!
    var timerRef: DatabaseReference!
    var gm = ""
    
    var room = ""
    var thisRoom : AccessoryTimer!
    var timerRunning = false
    var mzTimerLabel: MZTimerLabel!
    
    var codeEnteredSuccessfully: Bool = false
    
    @IBOutlet weak var codeEntryBottomConstant: NSLayoutConstraint!
    @IBOutlet weak var codeEntryTextField: UITextField!
    @IBOutlet weak var exitButtonBarHeight: NSLayoutConstraint!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var timerMessageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height
        self.codeEntryBottomConstant.constant = -screenHeight
        self.exitButtonBarHeight.constant = 0
        self.view.layoutIfNeeded()
        
        if let currentUserID = FirebaseUserManager().getCurrentUser()?.uid {
            
            ref = Database.database().reference().child("\(currentUserID)")
        }
        
        timerRef = ref.child("Current Games").child(self.room)
        
        ref.child("GameMasterPin").observe(.value) { (snap) in
            self.gm = ""
            
            if let code = snap.value as? String {
                self.gm = code
            }
        }
        
    
        self.timerRef.child("connectedAccessoryCount").observeSingleEvent(of: .value) { (snapshot) in
            
            var count = 1
            
            if let roomCount = snapshot.value as? NSNumber {
                count = roomCount.intValue + count
            }
            
            self.timerRef.child("connectedAccessoryCount").setValue(count)
            self.timerRef.child("AccessoryConnected").setValue("true")
        }
        
        timerMessageLabel.text = thisRoom.timerMessage
        
        // This is probably unnecessary but we don't want any wild observers running around.
        ref.removeAllObservers()
        setupTimerRefForSelectedRoom()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        if let enteredCode = textField.text {
            if enteredCode.lowercased() == thisRoom.defuseCode.lowercased() {
                
                self.codeEnteredSuccessfully = true
                
                if thisRoom.isGameWinningCondition == "true" {
                    if let timerLabel = self.mzTimerLabel {
                        let remainingTime = timerLabel.getCountDownTime()
                        self.timerRunning = false
                        self.ref.child("Current Games").child(self.thisRoom.parentGame).child("Win Time").setValue(remainingTime, andPriority: nil, withCompletionBlock: { (error, ref) in
                            self.ref.child("Current Games").child(self.thisRoom.parentGame).child("helpStatus").setValue("ConnectedGameOverWINNER")
                        })
                    }
                } else {
                    // TODO: - Maybe set some value to notify Game Master that code has been successfully entered.
                }

                timerMessageLabel.text = thisRoom.completionMessage
                timerMessageLabel.textColor = .white
            } else {
                
                let animation = CABasicAnimation(keyPath: "position")
                animation.duration = 0.07
                animation.repeatCount = 4
                animation.autoreverses = true
                animation.fromValue = NSValue(cgPoint: CGPoint(x: timerMessageLabel.center.x - 10, y: timerMessageLabel.center.y))
                animation.toValue = NSValue(cgPoint: CGPoint(x: timerMessageLabel.center.x + 10, y: timerMessageLabel.center.y))
                
                timerMessageLabel.layer.add(animation, forKey: "position")
            }
        }
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: - getKeyboardHeight
    @objc func keyboardWillShow(notification: Notification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height

        self.codeEntryTextField.text = ""
        self.codeEntryBottomConstant.constant = -keyboardHeight - 30
        self.exitButtonBarHeight.constant = 30
        self.exitButton.isHidden = false
        self.view.layoutIfNeeded()
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        // keyboard is dismissed/hidden from the screen
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height
        
        self.codeEntryBottomConstant.constant = -screenHeight - 30
        self.exitButtonBarHeight.constant = 0
        self.exitButton.isHidden = true
        self.view.layoutIfNeeded()
    }
    
    public func closeRoom() {
        self.ref.child("GameMasterPin").removeAllObservers()
        self.timerRef.child("Timer Info").removeAllObservers()
        
        self.timerRef.child("connectedAccessoryCount").removeAllObservers()
        self.timerRef.child("connectedAccessoryCount").observeSingleEvent(of: .value) { (snapshot) in
            
            var count = 0
            
            if let roomCount = snapshot.value as? NSNumber {
                count = roomCount.intValue - 1
            }
            
            self.timerRef.child("connectedAccessoryCount").setValue(max(0, count))
            
            if count > 0 {
                self.timerRef.child("AccessoryConnected").setValue("true")
            } else {
                self.timerRef.child("AccessoryConnected").setValue("false")
            }
        }
        print("unwound room successfully")
    }
    
    @IBOutlet weak var enterCodeButton: UIButton!
    @IBAction func enterCodeButtonPressed(_ sender: UIButton) {
        if codeEntryTextField.isFirstResponder {
           codeEntryTextField.resignFirstResponder()
        }
        codeEntryTextField.becomeFirstResponder()
    }
    
    @IBAction func closeCodeEntryButtonPressed(_ sender: UIButton) {
        codeEntryTextField.text = ""
        codeEntryTextField.resignFirstResponder()
    }
    
    @IBAction func exitButtonPressed(_ sender: UIButton) {
        
        if self.gm != "" {
            let storyBoard = UIStoryboard(name: "LockScreen", bundle: nil)
            if let lockViewController = storyBoard.instantiateInitialViewController() as? LockViewController {
                lockViewController.senderVC = self
                self.present(lockViewController, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: "Are you sure you want to exit \(thisRoom.parentGame)?", message: "This will return to the main menu. If you are playing \(thisRoom.parentGame), please hit CANCEL so you can continue your experience.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Exit Accessory", style: .default, handler: { (alert) in
                
                self.closeRoom()
                self.navigationController?.popViewController(animated: true)
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func createTimer(withTime time: TimeInterval,
                     forLabel textLabel: UILabel) {
        mzTimerLabel = MZTimerLabel.init(label: textLabel, andTimerType: MZTimerLabelTypeTimer)
        mzTimerLabel.resetTimerAfterFinish = false
        mzTimerLabel?.setCountDownTime(time)
    }
    
    func setupTimerRefForSelectedRoom() {
        ref.child("Games").queryOrdered(byChild: "name").queryEqual(toValue: thisRoom.parentGame).observeSingleEvent(of: .value) { (snapshot) in
            
            if let value = snapshot.value as? [String : Any] {
                if let game = value[value.keys.first!] as? [String: Any] {
                    if let timeLimit = game["timeLimit"] as? NSNumber {
                        self.thisRoom.parentGameTimeLimit = TimeInterval(truncating: timeLimit)
                    } else {
                        self.thisRoom.parentGameTimeLimit = 3600
                    }
                    
                    self.setupTimer()
                }
            }
        }
    }
    
    func setupTimer() {
        
        timerRef.child("Timer Info").removeAllObservers()
        timerRef.child("Timer Info").observe(.value) { (snapshot) in
            
            guard let snap = snapshot.value as? [String : Any] else {
                DispatchQueue.main.async {
                    
                    self.timerMessageLabel.text = self.thisRoom.timerMessage
                    self.timerMessageLabel.textColor = .white
                    
                    if let mzTimer = self.mzTimerLabel {
                        mzTimer.pause()
                        self.timerRunning = false
                        
                        mzTimer.setCountDownTime(self.thisRoom.parentGameTimeLimit)
                    } else {
                        self.createTimer(withTime: self.thisRoom.parentGameTimeLimit, forLabel: self.timerLabel)
                    }
                }
                return
            }
            
            if let running = snap["timerRunning"] as? NSNumber {
                
                DispatchQueue.main.async {
                    
                    guard let timeStamp = snap["timeStamp"] as? TimeInterval else { return }
                    guard let totalRunTime = snap["totalRunTime"] as? TimeInterval else { return }
                    
                    var timeToStart = totalRunTime - (Date().timeIntervalSince1970 - timeStamp)
                    
                    // TODO: REFACTOR THIS SHIZZZ
                    if running == 0 {
                        timeToStart = totalRunTime
                    }
                    
                    if let mzTimer = self.mzTimerLabel {
                        mzTimer.removeFromSuperview()
                    }
                    
                    self.timerLabel.text = ""
                    self.createTimer(withTime: timeToStart, forLabel: self.timerLabel)
                    
                    if running == 0 {
                        if let mzTimer = self.mzTimerLabel {
                            mzTimer.pause()
                            self.timerRunning = false
                        }
                    } else {
                        self.mzTimerLabel.start(ending: { (countTime) in
                            self.timerRunning = false
                        })
                        self.timerRunning = true
                    }
                }
            }
        }
    }
}
