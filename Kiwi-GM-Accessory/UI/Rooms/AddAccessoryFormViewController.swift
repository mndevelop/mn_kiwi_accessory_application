//
//  AddAccessoryFormViewController.swift
//  Kiwi-GM-Accessory
//
//  Created by Amir Nazari on 6/22/18.
//  Copyright © 2018 MN Develop LLC. All rights reserved.
//

import UIKit
import FirebaseDatabase

class AddAccessoryFormViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var parentNameField: UITextField!
    @IBOutlet weak var accessoryNameField: UITextField!
    @IBOutlet weak var selectGameForAccessoryButton: UIButton!
    @IBOutlet weak var isGameWinningSwitch: UISwitch!
    @IBOutlet weak var defuseCodeField: UITextField!
    @IBOutlet weak var defuseCodeEnabledSwitch: UISwitch!
    @IBOutlet weak var timerMessageField: UITextField!
    @IBOutlet weak var showTimerMessageSwitch: UISwitch!
    @IBOutlet weak var completionMessageField: UITextField!
    @IBOutlet weak var showCompletionMessageSwitch: UISwitch!
    @IBOutlet weak var saveAccessoryButton: UIButton!
    @IBOutlet weak var exitWithoutSavingButton: UIButton!
    @IBOutlet weak var parentGameTableView: UITableView!
    @IBOutlet weak var parentGameTableContainer: UIView!
    @IBOutlet weak var gameTableHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var cancelBarView: UIView!
    @IBOutlet weak var formContainerView: UIView!
    
    var ref : DatabaseReference!
    var selectedRoom: GameRoom!
    var gameList: [GameRoom] = []
    
    var accessoryList: [AccessoryTimer] = []
    var accessoryToEdit: AccessoryTimer?
    var selectedTheme = ""
    
    var formFields : [UITextField] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleUI()
        hideKeyboardWhenTappedAround()
        
        if let accessory = accessoryToEdit {
            parentNameField.text = accessory.parentGame
            accessoryNameField.text = accessory.title
            defuseCodeField.text = accessory.defuseCode
            completionMessageField.text = accessory.completionMessage
            timerMessageField.text = accessory.timerMessage
            
            var isGameWinning = false
            
            if accessory.isGameWinningCondition == "true" {
                isGameWinning = true
            }
            
            isGameWinningSwitch.setOn(isGameWinning, animated: true)
        }
        
        if let currentUserID = FirebaseUserManager().getCurrentUser()?.uid {
            
            ref = Database.database().reference().child("\(currentUserID)")
        }
        
        formFields.append(parentNameField)
        formFields.append(accessoryNameField)
        formFields.append(defuseCodeField)
        formFields.append(timerMessageField)
        formFields.append(completionMessageField)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupGameObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeFirebaseObservers()
    }
    
    func styleUI() {
        exitWithoutSavingButton.backgroundColor = UIColor.kiwiRed()
        exitWithoutSavingButton.layer.cornerRadius = 6.0
        
        saveAccessoryButton.backgroundColor = UIColor.kiwiGreen()
        saveAccessoryButton.layer.cornerRadius = 6.0
        
        selectGameForAccessoryButton.backgroundColor = UIColor.kiwiGreen()
        selectGameForAccessoryButton.applyDropShadow()
        
        for field in formFields {
            field.applyDropShadow()
        }
        
        gameTableHeightConstant.constant = 0
        view.setNeedsLayout()
        view.layoutIfNeeded()
        
        parentGameTableView.tableFooterView = UIView()
        parentGameTableView.allowsSelection = false
    }

    func listContainsGameTitle(title: String, parent: String, accessoryList: [AccessoryTimer]) -> Bool {
        
        for accessory in accessoryList {
            if accessory.title == title && accessory.parentGame == parent {
                return true
            }
        }
        
        return false
    }
    
    @IBAction func selectGameForAccessoryButtonPressed(_ sender: UIButton) {
        showGameList()
    }
    @IBOutlet weak var bottomOfContainerConstraint: NSLayoutConstraint!
    
    //MARK: - getKeyboardHeight
    @objc func keyboardWillShow(notification: Notification) {
        UIView.animate(withDuration: 1) {
            self.bottomOfContainerConstraint.constant = -52
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        bottomOfContainerConstraint.constant = 8
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }

    func showGameList() {
        
        self.view.endEditing(true)
        
        let adjustedConstant = formContainerView.bounds.height - 8
        self.cancelBarView.isHidden = false
        UIView.animate(withDuration: 0.7) {
            self.gameTableHeightConstant.constant = adjustedConstant
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    
    func hideGameList() {
        UIView.animate(withDuration: 0.7) {
            self.gameTableHeightConstant.constant = 0
            self.cancelBarView.isHidden = true
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func stopTimerSwitchValueChanged(_ sender: UISwitch) {
        
    }
    
    @IBAction func defuseCodeEnabledSwitchValueChanged(_ sender: UISwitch) {
        
    }
    
    @IBAction func showTimerSwitchValueChanged(_ sender: UISwitch) {
        
    }
    
    @IBAction func showCompletionMessageSwitchValueChanged(_ sender: UISwitch) {
        
    }
    
    @IBAction func cancelButtonGameListPressed(_ sender: UIButton) {
        hideGameList()
    }
    
    @IBAction func saveAccessoryButtonPressed(_ sender: UIButton) {
        storeEnteredValues()
    }
    
    @IBAction func exitWithoutSavingButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func storeEnteredValues() {
        if checkFields() {
            var accessoryObject : [String : String] = [:]
            
            guard let name = accessoryNameField.text else {
                let alert = UIAlertController(title: "Game Name is empty", message: "You cannot save a game without a name.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            var trimmedString = name.trimmingCharacters(in: .whitespaces)
            trimmedString = trimmedString.removingCharacters(inCharacterSet: .punctuationCharacters)
            trimmedString = trimmedString.removingCharacters(inCharacterSet: .illegalCharacters)
            
            if trimmedString.count <= 0 {
                let alert = UIAlertController(title: "Game Name is empty", message: "You cannot save a game without a name.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }

            accessoryObject["parentGame"] = parentNameField.text
            accessoryObject["title"] = trimmedString
            accessoryObject["defuseCode"] = defuseCodeField.text
            accessoryObject["timerMessage"] = timerMessageField.text
            accessoryObject["completionMessage"] = completionMessageField.text
            accessoryObject["gameWinningCondition"] = isGameWinningSwitch.isOn.description
            accessoryObject["themeImage"] = "accTheme1"
            accessoryObject["type"] = "defusable"
            
            if let editAccessory = accessoryToEdit {
                ref.child("AccessoryTimers").child(editAccessory.key).setValue(accessoryObject)
            } else {
                if listContainsGameTitle(title: trimmedString, parent: parentNameField.text!, accessoryList: accessoryList) {
                    
                    let alert = UIAlertController(title: "\(trimmedString) Already Exists", message: "You cannot save more than one accessory with the same name.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                
                ref.child("AccessoryTimers").childByAutoId().setValue(accessoryObject)
                
                let alert = UIAlertController(title: "Game Saved", message: "\(trimmedString) has been saved successfully! Tap OK to return to the game selection screen.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func clearFields() {
        for field in formFields {
            field.text = ""
        }
    }
    
    func checkFields() -> Bool {
        
        var allFilled = true
        
        for field in formFields {
            if let tf = field.text {
                
                let trimmed = tf.trimmingCharacters(in: .whitespacesAndNewlines)
                
                if trimmed.isEmpty {
                    allFilled = false
                }
            }
        }
        
        return allFilled
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == accessoryNameField {
            defuseCodeField.becomeFirstResponder()
        } else if textField == defuseCodeField {
            timerMessageField.becomeFirstResponder()
        } else if textField == timerMessageField {
            completionMessageField.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: GameSelectionTableViewCell.reuseID) as? GameSelectionTableViewCell else {
            return UITableViewCell()
        }
        
        cell.deleteButton.isHidden = true
        
        cell.roomButton.tag = indexPath.row
        cell.deleteButton.tag = indexPath.row
        cell.roomButton.setTitle(self.gameList[indexPath.row].name, for: .normal)
        
        cell.roomButton.addTarget(self, action: #selector(roomButtonTapped(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func roomButtonTapped(sender: UIButton) {
        selectedRoom = gameList[sender.tag]
        parentNameField.text = selectedRoom.name
        hideGameList()
    }
    
    func removeFirebaseObservers() {
        ref.child("Game Limit").removeAllObservers()
        ref.child("Games").removeAllObservers()
    }
    
    func setupGameObservers() {
        
        
        ref.child("Games").observe(.value, with: { (snapshot) in
            //if the reference have some values
            
            if snapshot.childrenCount > 0 {
                
                //clearing the list
                self.gameList.removeAll()
                
                //iterating through all the values
                for child in snapshot.children {
                    if let value = (child as? DataSnapshot)?.value as? [String : Any] {
                        
                        let gameRoom = GameRoom()
                        
                        if let snap = child as? DataSnapshot {
                            gameRoom.key = snap.key
                        }
                        
                        guard let name = value["name"] as? String else {
                            return
                        }
                        
                        gameRoom.name = name
                        
                        self.gameList.append(gameRoom)
                    }
                }
                
                DispatchQueue.main.async {
//                    self.animationStackViewContainer.isHidden = true
                    self.parentGameTableView.isHidden = false
                }
            } else {
                self.gameList = []
            }
            
            DispatchQueue.main.async {
                //reloading the tableview
                self.parentGameTableView.reloadData()
            }
        })
    }
}
